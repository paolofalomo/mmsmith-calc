<?php

namespace App\Service;


use App\Calculator\Calculation;
use App\Calculator\Errors\AbstractCalculatorError;

/**
 * Calculator service
 */
class CalculatorService {

    /**
     * Magic method to redirect to the correct operation
     * @param string $operation
     * @param array $arguments
     * @return Calculation
     * @throws AbstractCalculatorError
     */
    public function __call(string $operation, array $arguments): Calculation {
        return new Calculation($arguments[0], $operation);
    }

}