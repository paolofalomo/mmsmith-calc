<?php

namespace App\Controller;

use App\Calculator\Errors\AbstractCalculatorError;
use App\Service\CalculatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Calc controller
 */
class CalcController extends AbstractController {


    /**
     * Calculation page where user can fill the form
     * @Route("/", methods={"GET", "HEAD"})
     * @return Response
     */
    public function calcPage(): Response {
        return $this->render('base.html.twig', [
            'arguments' => [
                '', ''
            ],
            'operand' => 'sum',
            'has_errors' => false,
        ]);
    }

    /**
     * Calculate route when the user submit the form
     * @Route("/", methods={"POST"})
     * @param Request $request
     * @param CalculatorService $calculator
     * @return Response
     */
    public function calculate(Request $request, CalculatorService $calculator): Response {
        if ($this->isCsrfTokenValid('calc', $request->request->get('token'))) {
            $arguments = $request->get('arguments');
            $operand = $request->get('operand');
            $hasErrors = false;
            try {
                $calculation = $calculator->$operand($arguments);
                $result = $calculation;
            } catch (AbstractCalculatorError $error) {
                $hasErrors = true;
                $result = $error->getMessage();
            }

            return $this->render('base.html.twig', ['result' => $result,
                'arguments' => $arguments,
                'operand' => $operand,
                'has_errors' => $hasErrors
            ]);
        }
        return new Response('missing csfr token', 498);
    }
}