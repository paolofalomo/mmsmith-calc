<?php

namespace App\Calculator;
/**
 * Division operation implementation
 */
class Divide extends AbstractOperation {

    /**
     * @inheritDoc
     */
    public function getResult(): Result {
        try {
            $result = $this->arguments[0]->getValue() / $this->arguments[1]->getValue();
        } catch (\DivisionByZeroError $exception) {
            return new Result($this, "Cannot divide by 0" );
        }
        return new Result($this, "Result of division is: $result");
    }
}