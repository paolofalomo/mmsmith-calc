<?php

namespace App\Calculator;

/**
 * Result interface
 * Implement this interface to be sure a result is always available
 */
interface ResultInterface {

    /**
     * @return Result
     */
    public function getResult(): Result;
}