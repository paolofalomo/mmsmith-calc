<?php

namespace App\Calculator;

/**
 * Result class
 * @uses __toString() magic method
 */
class Result {
    /**
     * Operation that's been made
     * @var AbstractOperation
     */
    protected AbstractOperation $operation;

    /**
     * Result
     * @var string
     */
    protected string $result;

    /**
     * @param AbstractOperation $operation
     * @param string $result
     */
    public function __construct(AbstractOperation $operation, string $result) {
        $this->operation = $operation;
        $this->result = $result;
        return $this;
    }

    /**
     * __toString() magic method
     * @return string
     */
    public function __toString(): string {
        return $this->result;
    }
}