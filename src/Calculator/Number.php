<?php

namespace App\Calculator;

use App\Calculator\Errors\NonNumeric;

/**
 * Number class
 */
final class Number {

    /**
     * Final value
     * @var float
     */
    protected float $value;

    /**
     * @throws NonNumeric
     */
    public function __construct(string $val) {
        if (is_numeric($val)) {
            $this->value = floatval($val);
        } else {
            throw new NonNumeric($val);
        }
    }

    /**
     * Get the value of this number
     * @return float
     */
    public function getValue(): float {
        return $this->value;
    }
}