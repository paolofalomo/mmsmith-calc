<?php

namespace App\Calculator\Errors;
/**
 * Empty class to intercept all calculation errors
 */
class AbstractCalculatorError extends \Exception {

}