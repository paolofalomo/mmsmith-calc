<?php

namespace App\Calculator\Errors;

use Throwable;

/**
 * Too many arguments given
 */
class TooManyArguments extends  AbstractCalculatorError{

    public function __construct($message = "Too many arguments", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}