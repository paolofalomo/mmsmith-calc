<?php

namespace App\Calculator\Errors;

use Throwable;

/**
 * Non numeric
 * happens when a given value is not considered a number
 */
class NonNumeric extends AbstractCalculatorError {

    /**
     * @param string $nonNumber
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($nonNumber = "", $code = 0, Throwable $previous = null) {
        parent::__construct("This is not a number: $nonNumber", $code, $previous);
    }
}