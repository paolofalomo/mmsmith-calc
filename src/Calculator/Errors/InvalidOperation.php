<?php

namespace App\Calculator\Errors;

use Throwable;

/**
 * Invalid operation: this happens when an operation is unknown and considered invalid
 */
class InvalidOperation extends AbstractCalculatorError {

    public function __construct($operation, $code = 0, Throwable $previous = null) {
        parent::__construct("This is an invalid operation: $operation", $code, $previous);
    }
}