<?php

namespace App\Calculator\Errors;

use Throwable;

/**
 * Missing arguments
 * This happens when there aren't enough arguments to do the calculation
 */
class MissingArguments extends AbstractCalculatorError {

    public function __construct($message = "Not enough arguments", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}