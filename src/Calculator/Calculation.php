<?php

namespace App\Calculator;

use App\Calculator\Errors\InvalidOperation;
use function Symfony\Component\String\u;

class Calculation {

    protected Result $result;


    /**
     * @throws InvalidOperation
     */
    public function __construct($arguments, $operation) {
        $className = __NAMESPACE__ . '\\' . $this->operationToClassName($operation);
        if (class_exists($className) && is_subclass_of($className, ResultInterface::class)) {
            $calcOperation = new $className($arguments);
            $this->result = $calcOperation->getResult();
        } else {
            throw new InvalidOperation($operation);
        }
    }

    /**
     * @param $operation
     * @return string
     */
    private function operationToClassName($operation): string {
        return u($operation)->camel()->title()->toString();
    }

    public function __toString(): string {
        return $this->result;
    }
}