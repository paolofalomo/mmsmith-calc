<?php

namespace App\Calculator;


use App\Calculator\Errors\MissingArguments;
use App\Calculator\Errors\TooManyArguments;

/**
 * Abstract Operation
 * @uses Number
 * an arithmetical operation is considered
 */
abstract class AbstractOperation implements ResultInterface {

    /**
     * @var Number[]
     */
    public array $arguments;

    /**
     * @param string[] $arguments
     * @throws MissingArguments
     * @throws TooManyArguments
     */
    public function __construct(array $arguments) {
        $argsCount = count(array_filter($arguments, fn($arg) => $arg !== ''));
        if ($argsCount < 2) {
            throw new MissingArguments("$argsCount arguments given, 2 required");
        } elseif ($argsCount > 2) {
            throw new TooManyArguments("Too many arguments given ($argsCount), 2 required");
        }
        $this->arguments = array_map([$this, 'argumentToNumber'], $arguments);
        return $this;
    }

    /**
     * @throws Errors\NonNumeric
     */
    private function argumentToNumber($argument): Number {
        return new Number($argument);
    }

    /**
     * Method that gives back the result from calculation
     * @return Result
     */
    abstract public function getResult(): Result;
}