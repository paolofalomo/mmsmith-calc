<?php

namespace App\Calculator;
/**
 * Sum operation
 */
class Sum extends AbstractOperation {

    /**
     * @inheritDoc
     */
    public function getResult(): Result {
        $result = $this->arguments[0]->getValue() + $this->arguments[1]->getValue();
        return new Result($this, "Result of sum is: $result");
    }
}