<?php

namespace App\Calculator;

/**
 * Subtract operation
 */
class Subtract extends AbstractOperation {

    /**
     * @inheritDoc
     */
    public function getResult(): Result {
        $result = $this->arguments[0]->getValue() - $this->arguments[1]->getValue();
        return new Result($this, "Subtraction result is: $result");
    }
}