<?php

namespace App\Calculator;
/**
 * Multiply operation
 */
class Multiply extends AbstractOperation {

    /**
     * @inheritDoc
     */
    public function getResult(): Result {
        $result = $this->arguments[0]->getValue() * $this->arguments[1]->getValue();
        return new Result($this, "Multiplication result is: $result");
    }
}